﻿using System.Collections.Generic;

namespace SAK.Interfaces
{
    public interface IMessenger
    {
        #region Public Properties
        string Name { get; }
        #endregion

        #region Public Methods
        bool Send(string from, ICommunicationMessage message);

        void SetConfig();

        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SAK.Interfaces
{
    public interface IIntegrationMessage : IBaseMessage
    {
        #region Public Properties
        [DataMember]
        IDictionary<string, string> Header { get; set; }

        [DataMember]
        string MessageType { get; set; }

        [DataMember]
        string User { get; set; }
        #endregion

        #region Public Methods
        string ToXML();
        #endregion
    }
}

﻿using SAK.Entities;
using SAK.Enumerators;
using System.Collections.Generic;

namespace SAK.Interfaces
{
    public interface IChannel
    {
        #region Public Properties
        string Name { get; }

        event MessageEvent OnMessage;

        #endregion

        #region Public Methods
        bool IsOpen();

        bool Connect();

        bool Disconnect();

        IntegrationPostResponse Post(IIntegrationMessage message);

        void SetConfig();

        IIntegrationMessage MessageFactory();
        #endregion
    }
}
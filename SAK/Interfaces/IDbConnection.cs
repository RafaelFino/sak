﻿using SAK.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAK.Interfaces
{
    public interface IDbInterface
    {
        string ConnectionString { get;}

        IDataReader ExecuteReader(DbCommand command);
        int ExecuteNonQuery(DbCommand command);
        int ExecuteNonQuery(DbCommand command, out Dictionary<string, object> outputParameters);
        object ExecuteScalar(DbCommand command);
        object ExecuteScalar(DbCommand command, out Dictionary<string, object> outputParameters);
    }
}

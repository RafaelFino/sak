﻿using SAK.Config;
using System;
using System.Collections.Generic;

namespace SAK.Interfaces
{
    public interface ILogger : IDisposable
    {
        #region Public Properties
        LoggerConfigElement ConfigData { get; set; }
        #endregion

        #region Public Methods
        void AppError(string message);

        void Critical(string message);

        void Debug(string message);

        void Error(string message, Exception ex);
        void Info(string message);

        void Performance(string message);

        void SetConfig(LoggerConfigElement config);

        void Warning(string message);
        #endregion
    }
}
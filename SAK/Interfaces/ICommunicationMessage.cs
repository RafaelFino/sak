﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SAK.Interfaces
{
    public interface ICommunicationMessage : IBaseMessage
    {
        #region Public Properties
        [DataMember]
        Dictionary<string, byte[]> Attachs { get; set; }

        [DataMember]
        string Subject { get; set; }
        #endregion
    }
}
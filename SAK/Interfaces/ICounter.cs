﻿using System;

namespace SAK.Interfaces
{
    public interface IPerfCounter : IDisposable
    {
        #region Public Properties
        decimal AvarageTime { get; }

        long Counter { get; }

        long ElapsedTime { get; }

        long MaxTimeEvent { get; }

        long MinTimeEvent { get; }

        string Name { get; }

        decimal Ratio { get; }

        long Max { get; }

        long ActiveEvents { get; }
        #endregion

        #region Public Methods
        void Clear();

        string GetReport();

        void Increment(long elapsedTime, long value = 1);

        void IncrementEvents(long value = 1);

        void IncrementActiveEvents(long value = 1);

        void DecrementActiveEvents(long value = 1);

        void IncrementTime(long elapsedTime);

        void Restart();
        #endregion
    }
}
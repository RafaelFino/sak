﻿namespace SAK.Interfaces
{
    public interface ISerializer<T>
    {
        #region Public Methods
        T Deserialize(byte[] serializationBytes);

        T Deserialize(string data);

        byte[] Serialize(T item);

        string SerializeToString(T item);
        #endregion
    }
}
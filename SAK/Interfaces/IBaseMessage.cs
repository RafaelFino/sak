﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SAK.Interfaces
{
    public interface IBaseMessage
    {
        #region Public Properties
        [DataMember]
        string Data { get; set; }

        [DataMember]
        string From { get; set; }

        [DataMember]
        string To { get; set; }

        [DataMember]
        string ID { get; set; }

        [DataMember]
        DateTime MessageTime { get; set; }
        #endregion        
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAK.Enumerators
{
    public enum DbInterfaceType
    {
        InfrastructureInterface = 0,
        SQLInterface = 1,
        OracleInterface = 2
    }
}

﻿namespace SAK.Enumerators
{
    public enum IntegrationPostReturn
    {
        Success,
        Invalid,
        Error,
        ClosedChannel,
        ChannelNotFound,
        PostError
    }

    public enum QueueMode
    {
        Writer = 0,
        Listener = 1
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAK.Enumerators
{
    public enum SerializerTypes
    {
        DefaultXml = 0,
        Protobuf = 1
    }
}

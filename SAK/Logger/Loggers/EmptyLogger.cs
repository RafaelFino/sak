﻿using SAK.Config;
using SAK.Interfaces;
using System;
using System.Collections.Generic;

namespace SAK.Logger
{
    public class EmptyLogger : IBaseLogger
    {
        #region Public Methods
        public override void AppError(string message)
        {
            return;            
        }

        public override void Critical(string message)
        {
            return;
        }

        public override void Debug(string message)
        {
            return;
        }

        public override void Error(string message, Exception ex)
        {
            return;
        }

        public override void Info(string message)
        {
            return;
        }

        public override void Performance(string message)
        {
            return;
        }

        public override void Warning(string message)
        {
            return;
        }
        #endregion
    }
}
﻿using SAK.Config;
using SAK.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAK.Logger
{
    public abstract class IBaseLogger : ILogger
    {
        #region Private Fields
        private LoggerConfigElement _configData;
        #endregion

        #region Public Properties
        public LoggerConfigElement ConfigData
        {
            get { return _configData; }
            set { _configData = value; }
        }
        #endregion

        #region Public Methods
        public abstract void AppError(string message);
        public abstract void Critical(string message);
        public abstract void Debug(string message);
        

        public abstract void Error(string message, Exception ex);
        public abstract void Info(string message);
        public abstract void Performance(string message);

        public virtual void Dispose()
        {
        }

        public virtual void SetConfig(Config.LoggerConfigElement config)
        {
            _configData = config;
        }

        public abstract void Warning(string message);
        #endregion
    }
}

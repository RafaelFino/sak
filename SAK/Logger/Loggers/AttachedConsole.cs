﻿using SAK.Config;
using SAK.Interfaces;
using SAK.PerformanceMonitor;
using System;
using System.Collections.Generic;
using Pactual.EqDer.SAK;

namespace SAK.Logger
{
    public class AttachedConsole : IBaseLogger
    {

        #region Public Methods
        public override void Critical(string message)
        {
            Console.WriteLine("Critical: " + message);
        }

        public override void Debug(string message)
        {
            Console.WriteLine("Tracer: " + message);
        }

        public override void Dispose()
        {
            NativeConsole.DeattachConsole();
        }

        public override void Error(string message, Exception ex)
        {
            Console.WriteLine("Error:      " + message);
            Console.WriteLine("Exception : " + ex.ToString());
        }

        public override void AppError(string message)
        {
            Console.WriteLine("AppError:   " + message);
        }

        public override void Info(string message)
        {
            Console.WriteLine("Info: " + message);
        }
        public override void Performance(string message)
        {
            Console.WriteLine("Performance: " + message);
        }

        public override void SetConfig(LoggerConfigElement config)
        {
            NativeConsole.AttachConsole();
            ConfigData = config;
        }

        public override void Warning(string message)
        {
            Console.WriteLine("Warning: " + message);
        }
        #endregion
    }
}
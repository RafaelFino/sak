﻿using SAK.Config;
using SAK.Interfaces;
using SAK.PerformanceMonitor;
using System;
using System.Collections.Generic;

namespace SAK.Logger
{
    public class DBLogger : IBaseLogger
    {
        #region Public Methods
        public override void AppError(string message)
        {
            throw new NotImplementedException();
        }

        public override void Critical(string message)
        {
            throw new NotImplementedException();
        }

        public override void Debug(string message)
        {
            throw new NotImplementedException();
        }

        public override void Error(string message, Exception ex)
        {
            throw new NotImplementedException();
        }

        public override void Info(string message)
        {
            throw new NotImplementedException();
        }

        public override void Performance(string message)
        {
            throw new NotImplementedException();
        }

        public override void Warning(string message)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
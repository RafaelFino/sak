﻿using SAK.Config;
using SAK.Interfaces;
using SAK.PerformanceMonitor;
using System;
using System.Collections.Generic;

namespace SAK.Logger
{
    public class VSOutputLogger : IBaseLogger
    {

        #region Public Methods
        public override void AppError(string message)
        {
            if (System.Diagnostics.Debugger.IsAttached)
            {
                System.Diagnostics.Debug.WriteLine("ILogger.AppError:   " + message);
            }
        }

        public override void Critical(string message)
        {
            if (System.Diagnostics.Debugger.IsAttached)
            {
                System.Diagnostics.Debug.WriteLine("ILogger.Critical: " + message);
            }
        }

        public override void Debug(string message)
        {
            if (System.Diagnostics.Debugger.IsAttached)
            {
                System.Diagnostics.Debug.WriteLine("ILogger.Tracer: " + message);
            }
        }

        public override void Error(string message, Exception ex)
        {
            if (System.Diagnostics.Debugger.IsAttached)
            {
                System.Diagnostics.Debug.WriteLine("ILogger.Error:      " + message);
                System.Diagnostics.Debug.WriteLine("ILogger.Exception : " + ex.ToString());
            }

        }
        public override void Info(string message)
        {
            if (System.Diagnostics.Debugger.IsAttached)
            {
                System.Diagnostics.Debug.WriteLine("ILogger.Info: " + message);
            }
        }

        public override void Performance(string message)
        {
            if (System.Diagnostics.Debugger.IsAttached)
            {
                System.Diagnostics.Debug.WriteLine("ILogger.Performance: " + message);
            }
        }

        public override void Warning(string message)
        {
            if (System.Diagnostics.Debugger.IsAttached)
            {
                System.Diagnostics.Debug.WriteLine("ILogger.Warning: " + message);
            }
        }
        #endregion

    }
}
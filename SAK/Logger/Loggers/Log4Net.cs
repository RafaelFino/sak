﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAK.Config;

namespace SAK.Logger
{
    public class Log4Net : IBaseLogger
    {
        private static log4net.ILog _log;

        public override void AppError(string message)
        {
            _log.Warn("[AppError] " + message);
        }

        public override void Critical(string message)
        {
            _log.Fatal(message);
        }

        public override void Debug(string message)
        {
            _log.Debug(message);
        }

        public override void Error(string message, Exception ex)
        {
            _log.Error(message, ex);
        }

        public override void Info(string message)
        {
            _log.Info(message);
        }

        public override void Performance(string message)
        {
            _log.Debug("[Performance] " + message);
        }

        public override void SetConfig(LoggerConfigElement config)
        {
            _log = log4net.LogManager.GetLogger(config.Name);
        }

        public override void Warning(string message)
        {
            _log.Warn(message);
        }
    }
}


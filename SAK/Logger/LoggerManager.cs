﻿using SAK.Config;
using SAK.Interfaces;
using SAK.PerformanceMonitor;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace SAK.Logger
{
    public class LoggerManager
    {
        #region Private Fields
        private static LoggerManager _instance = new LoggerManager();
        private CancellationTokenSource _cancellationTokenSource;
        private ConcurrentQueue<Action> _data = new ConcurrentQueue<Action>();
        private ErrorMessagesFloodContention _errorFloodContention;
        private List<ILogger> _loggers;
        private AutoResetEvent _newLogEvent = new AutoResetEvent(false);
        private volatile bool _running = false;

        private bool _useErrorFloodContention = true;

        private Action<string> OnCritical;
        private Action<string> OnDebug;
        private Action<string, Exception> OnError;
        private Action<string> OnAppError;
        private Action<string> OnInfo;
        private Action<string> OnPerformance;
        private Action<string> OnWarning;
        #endregion

        #region Public Properties
        public static LoggerManager Instance
        {
            get
            {
                return LoggerManager._instance;
            }
        }

        public bool UseErrorFloodContention
        {
            get { return _useErrorFloodContention; }
            set { _useErrorFloodContention = value; }
        }
        #endregion

        #region Private Constructors
        private LoggerManager()
        {
            OnDebug += delegate { };
            OnCritical += delegate { };
            OnError += delegate { };
            OnAppError += delegate { };
            OnInfo += delegate { };
            OnPerformance += delegate { };
            OnWarning += delegate { };

            _loggers = new List<ILogger>();
            _errorFloodContention = new ErrorMessagesFloodContention(ref OnError);

            AddAppConfigLogger();
        }
        #endregion

        #region Public Methods
        public static ILogger CreateLogger(LoggerConfigElement config)
        {
            ILogger ret = null;
            try
            {
                ret = (ILogger)Activator.CreateInstance(
                                                        Type.GetType(config.Type),
                                                        new Object[] { }
                                                        );

                if (ret != null)
                {
                    ret.SetConfig(config);

                    if (config.SubscribePerformanceReport)
                    {
                        PerfMonitor.Instance.Report += ret.Performance;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerManager.Instance.Error(ex, "Error to try create an ILogger instance from {0} type", config.Type);
                throw;
            }

            return ret;
        }

        public void AddLogger(ILogger logger)
        {
            //Subscribe
            if (logger.ConfigData.LogDebug)
            {
                OnDebug += logger.Debug;
            }

            if (logger.ConfigData.LogCritical)
            {
                OnCritical += logger.Critical;
            }

            if (logger.ConfigData.LogError)
            {
                OnError += logger.Error;
            }

            if (logger.ConfigData.LogAppError)
            {
                OnAppError += logger.AppError;
            }

            if (logger.ConfigData.LogInfo)
            {
                OnInfo += logger.Info;
            }

            if (logger.ConfigData.LogPerformance)
            {
                OnPerformance += logger.Performance;
            }

            if (logger.ConfigData.LogWarning)
            {
                OnWarning += logger.Warning;
            }

            _loggers.Add(logger);
        }

        /// <summary>
        /// Adiciona uma entrada no log no nivel CRITICAL
        /// </summary>
        /// <param name="message">mensagem a ser adiconada no log</param>
        public void Critical(string message, params object[] args)
        {            
            if (OnCritical != null)
            {
                _data.Enqueue(() => OnCritical(string.Format(message, args)));
            }

            CheckState();
        }

        /// <summary>
        /// Adiciona uma entrada no log no nivel DEBUG
        /// </summary>
        /// <param name="message">mensagem a ser adiconada no log</param>
        public void Debug(string message, params object[] args)
        {            
            if (OnDebug != null)
            {
                _data.Enqueue(() => OnDebug(string.Format(message, args)));
            }

            CheckState();
        }

        /// <summary>
        /// Adiciona uma entrada no log no nivel ERROR
        /// </summary>
        /// <param name="message">mensagem a ser adiconada no log</param>
        /// <param name="ex">Exception a ser adiconada no log</param>
        public void Error(Exception ex, string message, params object[] args)
        {
            if (OnError != null)
            {
                _data.Enqueue(() =>
                {
                    string data = string.Format(message, args);

                    if (_useErrorFloodContention)
                    {
                        _errorFloodContention.Log(data, ex);
                        OnDebug(string.Format("{0} - Exception: {1}", data, ex.ToString()));
                    }
                    else
                    {
                        OnError(data, ex);
                    }
                });
            }

            CheckState();
        }


        /// <summary>
        /// Adiciona uma entrada no log no nivel ERROR
        /// </summary>
        /// <param name="message">mensagem a ser adiconada no log</param>
        public void AppError(string message, params object[] args)
        {
            if (OnAppError != null)
            {
                _data.Enqueue(() => OnAppError(string.Format(message, args)));
            }

            CheckState();
        }

        /// <summary>
        /// Adiciona uma entrada no log no nivel INFO
        /// </summary>
        /// <param name="message">mensagem a ser adiconada no log</param>
        public void Info(string message, params object[] args)
        {
            if (OnInfo != null)
            {
                _data.Enqueue(() => OnInfo(string.Format(message, args)));
            }

            CheckState();
        }
        /// <summary>
        /// Adiciona uma entrada no log no nivel Performance
        /// </summary>
        /// <param name="message">mensagem a ser adiconada no log</param>
        public void Performance(string message, params object[] args)
        {
            if (OnPerformance != null)
            {
                _data.Enqueue(() => OnPerformance(string.Format(message, args)));
            }

            CheckState();
        }

        /// <summary>
        /// Para a thread de escrita de log
        /// </summary>
        public void Stop()
        {
            if (_cancellationTokenSource != null)
            {
                _cancellationTokenSource.Cancel();
            }

            _newLogEvent.Set();
        }

        /// <summary>
        /// Adiciona uma entrada no log no nivel WARNING
        /// </summary>
        /// <param name="message">mensagem a ser adiconada no log</param>
        public void Warning(string message, params object[] args)
        {
            if (OnWarning != null)
            {
                _data.Enqueue(() => OnWarning(string.Format(message, args)));
            }

            CheckState();
        }
        #endregion

        #region Private Methods
        private void AddAppConfigLogger()
        {
            try
            {
                var loggers = ConfigManager.LoadLoggerConfig();

                if (loggers != null)
                {
                    foreach (LoggerConfigElement config in loggers.Values)
                    {
                        ILogger logger = CreateLogger(config);
                        AddLogger(logger);
                    }
                }
            }
            catch (Exception ex)
            {                
                throw new Exception("Fail to try load logger config from app config", ex);
            }            
        }

        /// <summary>
        /// Valida se a thread de gravacao de log está ativa caso nao esteja reinicia
        /// </summary>
        private void CheckState()
        {
            if (_running)
            {
                _newLogEvent.Set();
            }
            else
            {
                Stop();
                _cancellationTokenSource = new CancellationTokenSource();
                Task.Factory.StartNew((object o) => ConsumeQueue(_cancellationTokenSource), TaskCreationOptions.LongRunning, _cancellationTokenSource.Token);
            }
        }

        /// <summary>
        /// Consome a fila de mensagens para escrever no log
        /// </summary>
        /// <param name="tokenSource">Token para sinalizar o fim da gravação</param>
        private void ConsumeQueue(CancellationTokenSource tokenSource)
        {
            if (!_running && !tokenSource.IsCancellationRequested)
            {
                _running = true;

                try
                {
                    Action callback;
                    while (!tokenSource.IsCancellationRequested)
                    {
                        while (_data.TryDequeue(out callback))
                        {
                            if (callback != null)
                                callback();
                        }

                        _newLogEvent.WaitOne();
                    }
                }
                finally
                {
                    _running = false;
                    _cancellationTokenSource = new CancellationTokenSource();
                }
            }
        }
        #endregion

    }
}
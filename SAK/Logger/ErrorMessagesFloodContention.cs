﻿using System;
using System.Collections.Concurrent;

namespace SAK.Logger
{
    public class ErrorMessage
    {
        #region Public Properties
        public long Count { get; set; }
        public DateTime Date { get; set; }
        public bool WriteErrorEvent { get; set; }
        #endregion

        #region Public Constructors
        public ErrorMessage()
        {
            Date = DateTime.Now;
            Count = 1;
            WriteErrorEvent = true;
        }

        #endregion
    }

    public class ErrorMessagesFloodContention
    {
        #region Public Fields
        /// <summary>
        /// Number of similar messages in perdiod that will be written as an error message (event counter)
        /// </summary>
        public const long FLOOD_CONTENTION_LIMIT = 15;

        /// <summary>
        /// Interval to retain similar error messages (minutes)
        /// </summary>
        public const long FLOOD_CONTENTION_TIME = 5;

        #endregion

        #region Private Fields
        private Action<string, Exception> _callback;

        private ConcurrentDictionary<int, ErrorMessage> errors = new ConcurrentDictionary<int, ErrorMessage>();
        #endregion

        #region Public Constructors
        public ErrorMessagesFloodContention(ref Action<string, Exception> callback)
        {
            _callback = callback;
        }

        #endregion

        #region Public Methods
        //Eventos durante o periodo
        /// <summary>
        /// Log messages with flood contetion, all messages will be written as a tracer message but only new error messages will be written as an error messages.
        /// </summary>
        /// <param name="message">Message to log</param>
        /// <param name="ex">Exception to log</param>
        public void Log(string message, Exception ex)
        {
            int hashCode = message.GetHashCode();

            //Toda lógica de tratamento de contenção dentro do update do ConcurrentDictionary
            ErrorMessage error = errors.AddOrUpdate(hashCode,
                new ErrorMessage(),
                (k, v) =>
                {
                    //Caso seja a primeira entrada após FLOOD_CONTENTION_TIME minutos ou mais da ultima, irá criar uma entrada nova e logará
                    if ((DateTime.Now - v.Date).Minutes > FLOOD_CONTENTION_TIME)
                    {
                        //Novo ponto de analise, WriteErrorEvent = true
                        v = new ErrorMessage();
                    }
                    else
                    {
                        //Não passaram FLOOD_CONTENTION_LIMIT minutos do ultimo evento
                        //De qualquer maneira, soma uma entrada no objeto
                        v.Count++;

                        //FloodContention -> Só irá logar caso seja uma das FLOOD_CONTENTION_LIMIT primeiras mensagens de erro do mesmo tipo nos ultimos 5 minutos
                        v.WriteErrorEvent = (v.Count < FLOOD_CONTENTION_LIMIT);
                    }

                    return v;
                });

            //Verifica se é para logar erro
            if (error.WriteErrorEvent)
            {
                //loga erro e envia email
                if (_callback != null)
                {
                    _callback(message, ex);
                }
            }
        }

        #endregion
    }
}
﻿using SAK.Logger;
using System;
using System.ServiceModel;

namespace SAK.WCF
{
    public class WCFServiceController
    {
        #region Public Fields
        public Action<CommunicationState> OnStatusChange;
        #endregion

        #region Private Fields
        private ServiceHost _service;
        private string _serviceName;
        private Type _type;
        #endregion

        #region Public Properties
        public string ServiceName
        {
            get { return _serviceName; }
            set { _serviceName = value; }
        }
        #endregion

        #region Public Constructors
        public WCFServiceController(object serviceImp, string serviceName = "")
        {
            _type = serviceImp.GetType();

            if (!string.IsNullOrEmpty(serviceName))
            {
                _serviceName = serviceName;
            }
            else
            {
                _serviceName = _type.ToString();
            }
        }

        #endregion

        #region Public Methods
        public void Start()
        {
            try
            {
                _service = new ServiceHost(_type);

                _service.Closed += StatusChange;
                _service.Closing += StatusChange;
                _service.Faulted += StatusChange;
                _service.Opened += StatusChange;
                _service.Opening += StatusChange;

                _service.Open();

                LoggerManager.Instance.Debug("[WCFServiceController.Start] [Service " + _serviceName + "] Started!");
            }
            catch (Exception ex)
            {
                LoggerManager.Instance.Error(ex, "[WCFServiceController.Start] Error to try start " + _serviceName);
                throw;
            }
        }

        public void Stop()
        {
            try
            {
                if (_service != null)
                {
                    _service.Close();

                    LoggerManager.Instance.Debug("[WCFServiceController.Stop] [Service " + _serviceName + "] Stopped!");
                }
            }
            catch (Exception ex)
            {
                LoggerManager.Instance.Error(ex, "[WCFServiceController.Stop] Error to try stop " + _serviceName);
                throw;
            }
        }

        #endregion

        #region Private Methods
        private void StatusChange(object sender, EventArgs e)
        {
            if (_service != null)
            {
                LoggerManager.Instance.Debug("[WCFServiceController.StatusChange] [Service " + _serviceName + "] Status changed to " + _service.State);

                if (OnStatusChange != null)
                {
                    try
                    {
                        OnStatusChange(_service.State);
                    }
                    catch (Exception ex)
                    {
                        LoggerManager.Instance.Error(ex, "[WCFServiceController.StatusChange] Error to try execute callback to " + _serviceName);
                    }
                }
            }
        }

        #endregion
    }
}
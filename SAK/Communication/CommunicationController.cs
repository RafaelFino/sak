﻿using SAK.Interfaces;
using SAK.Logger;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace SAK.Communication
{
    public class CommunicationController
    {
        #region Private Fields
        private static CommunicationController _instance = new CommunicationController();
        private ConcurrentDictionary<string, IMessenger> _messengers = new ConcurrentDictionary<string, IMessenger>();
        #endregion

        #region Public Properties
        public static CommunicationController Instance
        {
            get
            {
                return _instance;
            }
        }
        #endregion

        #region Private Constructors
        private CommunicationController()
        {
        }

        #endregion

        #region Public Methods
        public bool AddMessenger(IMessenger messenger)
        {
            return _messengers.TryAdd(messenger.Name, messenger);
        }

        public bool CreateMessenger(string messengerName, string fromType)
        {
            bool ret = false;
            try
            {
                IMessenger messenger = (IMessenger)Activator.CreateInstance(
                                                               Type.GetType(fromType),
                                                               new Object[] { messengerName }
                                                               );

                if (messenger != null)
                {
                    messenger.SetConfig();
                    ret = AddMessenger(messenger);
                }
            }
            catch (Exception ex)
            {
                LoggerManager.Instance.Error(ex, "Error to try create an IMessenger instance to {0}", fromType);
            }

            return ret;
        }

        public bool Send(string messengerName, ICommunicationMessage message)
        {
            bool ret = false;

            try
            {
                IMessenger messenger;
                if (_messengers.TryGetValue(messengerName, out messenger))
                {
                    ret = messenger.Send(messengerName, message);
                }
            }
            catch (Exception ex)
            {
                LoggerManager.Instance.Error(ex, "Error to try post message {0} on messenger {1}", message.Data, messengerName);
            }

            return ret;
        }

        #endregion
    }
}
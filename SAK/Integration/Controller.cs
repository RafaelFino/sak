﻿using SAK.Config;
using SAK.Entities;
using SAK.Enumerators;
using SAK.Interfaces;
using SAK.Logger;
using SAK.PerformanceMonitor;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace SAK.Integration
{
    public class IntegrationController : IDisposable
    {

        #region Public Fields
        public Action<string, IIntegrationMessage> MessageReceived;
        #endregion

        #region Private Fields
        private static IntegrationController _instance = new IntegrationController();
        private ConcurrentDictionary<string, IChannel> _channels = new ConcurrentDictionary<string, IChannel>();
        private PerfCounterType _counterType = PerfCounterType.Empty;
        #endregion

        #region Public Properties
        public static IntegrationController Instance
        {
            get
            {
                return _instance;
            }
        }
        public IEnumerable<string> ActiveChannels
        {
            get
            {
                return _channels.Where(p => p.Value.IsOpen()).Select(p => p.Key);
            }
        }
        public PerfCounterType CounterType
        {
            get { return _counterType; }
            set { _counterType = value; }
        }
        #endregion

        #region Private Constructors
        private IntegrationController()
        {
            MessageReceived += delegate { };
        }
        #endregion

        #region Public Methods
        public bool AddChannel(IChannel channel)
        {
            bool ret = true;
            IChannel oldChannel;

            if (_channels.TryGetValue(channel.Name, out oldChannel))
            {
                if (oldChannel.IsOpen())
                {
                    ret = false;
                }
            }

            if (ret)
            {
                _channels.AddOrUpdate(channel.Name, channel, (k, v) => channel);
                channel.OnMessage += DispatchMessage;
                channel.Connect();
            }

            return ret;
        }

        public bool CreateChannel(string channelName, string fromType = "")
        {
            bool ret = false;
            try
            {
                Type queueType = null;

                if (string.IsNullOrEmpty(fromType))
                {
                    MQConfigElement config = ConfigManager.LoadWMQConfig(channelName);

                    if (config != null)
                    {
                        queueType = Type.GetType(config.QueueType, false);
                        if (queueType == null)
                        {
                            string assemblyName = "BTG.Shared.Integration.Channels." + config.QueueType;
                            queueType = Type.GetType(assemblyName, false);
                        }
                    }
                    else
                    {
                        LoggerManager.Instance.AppError("[IntegrationController.CreateChannel] Cannot find implementation for this channel: {0}", channelName);
                    }
                }
                else
                {
                    queueType = Type.GetType(fromType, false);
                }

                if (queueType != null)
                {
                    IChannel channel = (IChannel)Activator.CreateInstance(
                                                                   queueType,
                                                                   new Object[] { channelName }
                                                                   );
                    if (channel != null)
                    {
                        channel.SetConfig();
                        ret = AddChannel(channel);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerManager.Instance.Error(ex, "[IntegrationController.CreateChannel] Error to try create an IChannel instance to {0}.{1}", fromType, channelName);
            }

            return ret;
        }

        public void Dispose()
        {
            Stop();
        }

        public IChannel GetChannel(string channelName)
        {
            IChannel channel = null;
            try
            {
                if (_channels.TryGetValue(channelName, out channel))
                {
                    if (channel == null || !channel.IsOpen())
                    {
                        if (_channels.TryRemove(channelName, out channel))
                        {
                            string queueType = string.Empty;

                            if (channel != null)
                            {
                                queueType = channel.GetType().FullName;
                            }

                            if (CreateChannel(channelName, queueType))
                            {
                                channel = GetChannel(channelName);
                            }
                        }
                    }
                }
                else
                {
                    if (CreateChannel(channelName))
                    {
                        channel = GetChannel(channelName);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerManager.Instance.Error(ex, "Error to try get channel {0}", channelName);
                channel = null;
            }

            return channel;
        }

        public bool IsActive(string channel)
        {
            return _channels.Where(p => p.Value.IsOpen()).Select(p => p.Key).Contains(channel);
        }

        public IntegrationPostResponse Post(string channelName, IIntegrationMessage message)
        {
            IntegrationPostResponse ret = new IntegrationPostResponse(channelName);

            try
            {
                using (IPerfCounter counter = PerfMonitor.CreateCounter(channelName + ".Post"))
                {
                    IChannel channel = GetChannel(channelName);
                    if (channel != null)
                    {
                        if (channel.IsOpen())
                        {
                            ret = channel.Post(message);
                        }
                        else
                        {
                            ret.Status = IntegrationPostReturn.ClosedChannel;
                        }
                    }
                    else
                    {
                        ret.Status = IntegrationPostReturn.ChannelNotFound;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerManager.Instance.Error(ex, "Error to try post message {0} on channel {1}", message.Data, channelName);
            }

            return ret;
        }

        public Dictionary<string, IntegrationPostResponse> Post(string[] channels, IIntegrationMessage message)
        {
            Dictionary<string, IntegrationPostResponse> ret = new Dictionary<string, IntegrationPostResponse>();

            using (IPerfCounter counter = PerfMonitor.CreateCounter("Multiple.Post"))
            {
                foreach (var channel in channels)
                {
                    try
                    {
                        ret.Add(channel, Post(channel, message));
                        counter.IncrementEvents();
                    }
                    catch (Exception ex)
                    {
                        LoggerManager.Instance.Error(ex, "Error to try post message {0} on channel {1}", message.Data, channel);
                    }
                }
            }

            return ret;
        }

        public Dictionary<string, IntegrationPostResponse> PostBroadcast(IIntegrationMessage message)
        {
            Dictionary<string, IntegrationPostResponse> ret = new Dictionary<string, IntegrationPostResponse>();

            using (IPerfCounter counter = PerfMonitor.CreateCounter("PostBroadcast"))
            {
                foreach (var channel in _channels)
                {
                    try
                    {
                        ret.Add(channel.Key, channel.Value.Post(message));
                        counter.IncrementEvents();
                    }
                    catch (Exception ex)
                    {
                        LoggerManager.Instance.Error(ex, "Error to try post message {0} on channel {1}", message.Data, channel.Key);
                    }
                }
            }

            return ret;
        }

        public void Stop()
        {
            foreach (IChannel channel in _channels.Values)
            {
                channel.Disconnect();
            }
        }

        public bool SubscribeChannel(string channelName, Action<string, IIntegrationMessage> callback)
        {
            bool ret = false;
            try
            {
                IChannel channel = GetChannel(channelName);
                if (channel != null)
                {
                    ret = true;

                    if (!MessageReceived.GetInvocationList().Contains(callback))
                    {
                        MessageReceived += callback;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerManager.Instance.Error(ex, "[IntegrationController.SubscribeChannel] Error to try subscribe channel {0}", channelName);
            }

            return ret;
        }
        #endregion

        #region Private Methods
        private void DispatchMessage(string channelName, IIntegrationMessage message)
        {
            using (IPerfCounter counter = PerfMonitor.CreateCounter(channelName + ".DispatchMessage"))
            {
                try
                {
                    MessageReceived(channelName, message);
                }
                catch (Exception ex)
                {
                    LoggerManager.Instance.Error(ex, "Error to try call message callback -> message {0} on channel {1}", message, channelName);
                }
            }
        }
        #endregion

    }
}
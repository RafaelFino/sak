﻿using SAK.Config;
using SAK.Entities;
using SAK.Enumerators;
using SAK.Interfaces;
using SAK.Logger;
using SAK.Serializer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace SAK.Integration.Channels
{
    public class MSMQ : IntegrationBaseChannel
    {
        #region Private Classes
        private class Formatter : IMessageFormatter
        {
            #region Public Methods
            public bool CanRead(System.Messaging.Message message)
            {
                return true;
            }

            public object Clone()
            {
                return this.MemberwiseClone();
            }

            public object Read(System.Messaging.Message message)
            {
                System.IO.StreamReader sr = new System.IO.StreamReader(message.BodyStream, System.Text.Encoding.UTF8);
                string retorno = sr.ReadToEnd();
                return retorno;
            }

            public void Write(System.Messaging.Message message, object obj)
            {
                System.IO.MemoryStream mem = new System.IO.MemoryStream();
                System.IO.StreamWriter sw = new System.IO.StreamWriter(mem, System.Text.Encoding.UTF8);
                sw.Write(obj);
                // obj neste caso sempre vai ser uma string
                sw.Flush();
                mem.Seek(0, System.IO.SeekOrigin.Begin);
                message.BodyStream = mem;
            }
            #endregion
        }
        #endregion

        #region Public Fields
        public MessageEnumerator _queueMessageEnumerator;
        public ISerializer<IIntegrationMessage> _serializer;
        #endregion

        #region Private Fields
        private string _address;
        private MessageQueue _queue;
        #endregion

        #region Public Events
        public override event MessageEvent OnMessage;
        #endregion

        #region Public Constructors
        public MSMQ(string name)
            : base(name)
        {
        }
        #endregion

        #region Public Methods
        public override bool Connect()
        {
            try
            {
                if (!MessageQueue.Exists(_address))
                {
                    _queue = MessageQueue.Create(_address);
                }
                else
                {
                    _queue = new MessageQueue(_address);
                }

                _queueMessageEnumerator = _queue.GetMessageEnumerator2();

                _queue.ReceiveCompleted += OnReceive;

                if (Mode == QueueMode.Listener)
                {
                    _queue.BeginReceive();
                }

                return true;
            }
            catch (Exception ex)
            {
                LoggerManager.Instance.Error(ex, "[MSMQ.Connect] Fail to try Connect on queue");
                throw;
            }
        }

        public override bool Disconnect()
        {
            try
            {
                _queueMessageEnumerator.Reset();
                _queueMessageEnumerator.Close();
                _queue.Close();
                _queue = null;

                return true;
            }
            catch (Exception ex)
            {
                LoggerManager.Instance.Error(ex, "[MSMQ.Disconnect] Fail to try Disconnect from queue");
                throw;
            }
        }

        public override bool IsOpen()
        {
            if (_queue != null)
            {
                return _queue.CanRead | _queue.CanWrite;
            }

            return false;
        }

        public override Interfaces.IIntegrationMessage MessageFactory()
        {
            return new IntegrationMessage();
        }

        public override IntegrationPostResponse Post(Interfaces.IIntegrationMessage message)
        {
            IntegrationPostResponse ret = new IntegrationPostResponse(Name);
            try
            {
                using (Message msg = new System.Messaging.Message())
                {
                    msg.Formatter = new Formatter();
                    msg.Body = _serializer.SerializeToString(message);
                    msg.Label = message.MessageType;
                    msg.CorrelationId = message.ID;

                    ret.MessageId = msg.Id;

                    _queue.Send(msg);

                    ret.Status = IntegrationPostReturn.Success;
                }
            }
            catch (Exception ex)
            {
                LoggerManager.Instance.Error(ex, "[MSMQ.Post] Fail to try Post a message");
                throw;
            }

            return ret;
        }

        public override void SetConfig()
        {
            MQConfigElement config = ConfigManager.LoadWMQConfig(Name);

            if (config == null)
            {
                throw new Exception(string.Format("[MSMQ.SetConfig] Configuration not found for queue {0}", Name));
            }

            _mode = config.Mode;
            _address = config.Host;

            switch (config.Serializer)
            {
                case SerializerTypes.Protobuf:
                    {
                        _serializer = new ProtobufSerializer<IIntegrationMessage>();
                        break;
                    }
                default:
                    {
                        _serializer = new DefaultXmlSerializer<IIntegrationMessage>();
                        break;
                    }
            }
        }
        #endregion

        #region Private Methods
        private void OnReceive(object sender, ReceiveCompletedEventArgs e)
        {
            try
            {
                Message msg = e.Message;

                IIntegrationMessage newMessage = MessageFactory();

                newMessage = _serializer.Deserialize((string)msg.Body);
                OnMessage(Name, newMessage);

                _queue.BeginReceive();
            }
            catch (MessageQueueException mqEx)
            {
                LoggerManager.Instance.AppError("[MSMQ.OnReceive] MQ Exception: {0}", mqEx.ToString());
                throw;
            }
            catch (Exception ex)
            {
                LoggerManager.Instance.Error(ex, "[MSMQ.OnReceive] Fail to try send a new message received");
                throw;
            }
        }
        #endregion
    }
}

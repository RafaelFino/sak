﻿using SAK.Config;
using SAK.Entities;
using SAK.Enumerators;
using SAK.Interfaces;
using SAK.Logger;
using SAK.PerformanceMonitor;
using SAK.Serializer;
//using IBM.WMQ;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace SAK.Integration.Channels
{
    /*
    public class WMQ : IntegrationBaseChannel
    {
        #region Protected Fields
        protected int _openQueueOptions;
        protected MQQueueManager _qm = null;
        protected MQQueue _queue = null;
        protected ISerializer<IIntegrationMessage> _serializer;
        #endregion

        #region Private Fields
        private bool _autoCommit = true;
        private string _channel;
        private MQGetMessageOptions _gmo = new MQGetMessageOptions();
        private string _host;
        private int _port;
        private string _queueManagerName;
        private string _queueName;
        #endregion

        #region Public Events
        public override event MessageEvent OnMessage;
        #endregion

        #region Public Properties
        public bool AutoCommit
        {
            get { return _autoCommit; }
            set { _autoCommit = value; }
        }
        public string Channel
        {
            get { return _channel; }
            set { _channel = value; }
        }
        public int CurrentDepth
        {
            get
            {
                if (IsOpen())
                {
                    return _queue.CurrentDepth;
                }

                return -1;
            }
        }
        public string Host
        {
            get { return _host; }
            set { _host = value; }
        }
        public int Port
        {
            get { return _port; }
            set { _port = value; }
        }
        public string QueueManagerName
        {
            get { return _queueManagerName; }
            set { _queueManagerName = value; }
        }
        public string QueueName
        {
            get { return _queueName; }
            set { _queueName = value; }
        }
        #endregion

        #region Public Constructors
        public WMQ(string name)
            : base(name)
        {
            OnMessage += delegate { };
        }
        #endregion

        #region Public Methods
        public void Commit()
        {
            if (_qm != null)
            {
                _qm.Commit();
            }
        }

        public override bool Connect()
        {
            bool ret = false;

            try
            {
                if (IsOpen())
                {
                    Disconnect();
                }

                if (Mode == QueueMode.Writer)
                {
                    _openQueueOptions = MQC.MQOO_FAIL_IF_QUIESCING | MQC.MQOO_OUTPUT;
                }
                else //Listener
                {
                    _openQueueOptions = MQC.MQOO_INPUT_AS_Q_DEF | MQC.MQOO_FAIL_IF_QUIESCING;

                    _gmo.Options = MQC.MQGMO_WAIT;
                    _gmo.MatchOptions = MQC.MQMO_MATCH_CORREL_ID;
                    _gmo.WaitInterval = MQC.MQWI_UNLIMITED;
                }

                Hashtable props = new Hashtable();
                props.Add(MQC.HOST_NAME_PROPERTY, _host);
                props.Add(MQC.CHANNEL_PROPERTY, _channel);
                props.Add(MQC.PORT_PROPERTY, _port);
                props.Add(MQC.TRANSPORT_PROPERTY, MQC.TRANSPORT_MQSERIES_MANAGED);

                _qm = new MQQueueManager(_queueManagerName, props);

                if (_qm.IsConnected)
                {
                    _queue = _qm.AccessQueue(_queueName, _openQueueOptions);

                    ret = _queue.IsOpen;

                    if (ret && Mode == QueueMode.Listener)
                    {
                        ThreadPool.QueueUserWorkItem((x) => StartToListen());
                    }
                }
            }
            catch (MQException mqe)
            {
                LoggerManager.Instance.Error(mqe, "MQ Exception on try connect MQException: {0} ReasonCode: {1}", mqe.Message, mqe.ReasonCode);
                throw;
            }
            catch (Exception ex)
            {
                LoggerManager.Instance.Error(ex, "Fail to try connect on queueManager");
                throw;
            }

            return ret;
        }

        public override bool Disconnect()
        {
            bool ret = false;
            try
            {
                if (_queue.IsOpen)
                {
                    _queue.Close();
                }

                if (_qm != null)
                {
                    _qm.Disconnect();
                }

                ret = _qm.IsConnected;
            }
            catch (Exception ex)
            {
                LoggerManager.Instance.Error(ex, "Fail to try disconnect from queueManager");
                throw;
            }

            return ret;
        }

        public override bool IsOpen()
        {
            if (_qm == null || _queue == null)
            {
                return false;
            }

            if (_qm.IsConnected)
            {
                if (_queue.IsOpen)
                {
                    return true;
                }
            }

            return false;
        }

        public override IIntegrationMessage MessageFactory()
        {
            return new IntegrationMessage();
        }

        public override IntegrationPostResponse Post(IIntegrationMessage message)
        {
            IntegrationPostResponse ret = new IntegrationPostResponse(Name);
            try
            {
                if (!IsOpen())
                {
                    ret.Status = IntegrationPostReturn.PostError;
                    Disconnect();
                    return ret;
                }

                using (IPerfCounter counter = PerfMonitor.CreateCounter("WMQWriter.Put"))
                {
                    MQMessage msg = FormatMessage(message);

                    _queue.Put(msg);

                    ret.MessageId = System.Text.Encoding.UTF8.GetString(msg.MessageId);
                    ret.Status = IntegrationPostReturn.Success;
                }
            }
            catch (MQException mqe)
            {
                LoggerManager.Instance.Error(mqe, "[WMQWriter.Post] MQ Exception on try send a message MQException: {0} ReasonCode: {1}", mqe.Message, mqe.ReasonCode);
                Disconnect();
                throw;
            }
            catch (Exception ex)
            {
                LoggerManager.Instance.Error(ex, "[WMQWriter.Post] Fail to try send a message");
                throw;
            }

            return ret;
        }

        public int QueueDepth()
        {
            int ret = -1;

            if (_qm != null)
            {
                if (_qm.IsConnected)
                {
                    try
                    {
                        using (IPerfCounter counter = PerfMonitor.CreateCounter("WMQBase.QueueDepth"))
                        {
                            using (MQQueue queueInfo = _qm.AccessQueue(this.Name, MQC.MQOO_INQUIRE))
                            {
                                ret = queueInfo.CurrentDepth;
                                queueInfo.Close();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(string.Format("Error to try open queue for get info{0}", Name), ex);
                    }
                }
            }

            return ret;
        }

        public void Rollback()
        {
            if (_qm != null)
            {
                _qm.Backout();
            }
        }

        public override void SetConfig()
        {
            MQConfigElement config = ConfigManager.LoadWMQConfig(Name);

            if (config == null)
            {
                throw new Exception(string.Format("Configuration not found for queue {0}", Name));
            }            

            switch (config.Serializer)
            {
                case SerializerTypes.Protobuf:
                    {
                        _serializer = new ProtobufSerializer<IIntegrationMessage>();
                        break;
                    }
                default:
                    {
                        _serializer = new DefaultXmlSerializer<IIntegrationMessage>();
                        break;
                    }
            }            

            _channel = config.Channel;
            _queueManagerName = config.QueueManager;
            _queueName = config.Queue;
            _host = config.Host;
            _port = Convert.ToInt32(config.Port);

            _mode = config.Mode;
            _autoCommit = Convert.ToBoolean(config.AutoCommit);
        }

        public void StartToListen()
        {
            while (IsOpen())
            {
                MQMessage message = new MQMessage();

                try
                {
                    IIntegrationMessage newMsg = null;

                    using (IPerfCounter counter = PerfMonitor.CreateCounter("WMQListener.Get"))
                    {
                        _queue.Get(message, _gmo);

                        newMsg = ParserMessage(message);
                    }

                    if (newMsg != null)
                    {
                        OnMessage(Name, newMsg);
                    }

                    if (AutoCommit)
                    {
                        Commit();
                    }
                }
                catch (MQException mqe)
                {
                    LoggerManager.Instance.Error(mqe, "[WMQListener.Post] MQ Exception: {0} ReasonCode: {1}", mqe.Message, mqe.ReasonCode);
                }
                catch (Exception ex)
                {
                    LoggerManager.Instance.Error(ex, "[WMQListener.Post] Fail to try get a message");
                }
            }
        }
        #endregion

        #region Protected Methods
        protected MQMessage FormatMessage(IIntegrationMessage message)
        {
            MQMessage ret = new MQMessage();
            string data = _serializer.SerializeToString(message);
            ret.WriteUTF(data);

            return ret;
        }

        protected IIntegrationMessage ParserMessage(MQMessage message)
        {
            try
            {
                string data = message.ReadUTF();
                IIntegrationMessage ret = _serializer.Deserialize(data);

                return ret;
            }
            catch (Exception ex)
            {
                LoggerManager.Instance.Error(ex, "Fail to try parser a message");
                throw;
            }
        }
        #endregion
        
    }
     * */
}
﻿using SAK.Entities;
using SAK.Enumerators;
using SAK.Interfaces;
using System.Collections.Generic;

namespace SAK.Integration
{
    public abstract class IntegrationBaseChannel : IChannel
    {
        #region Private Fields
        private string _name;
        protected QueueMode _mode = QueueMode.Writer;

        public QueueMode Mode
        {
            get { return _mode; }
            set { _mode = value; }
        }
        #endregion

        #region Public Events
        public virtual event MessageEvent OnMessage;
        #endregion

        #region Public Properties
        public string Name
        {
            get { return _name; }
        }
        #endregion

        #region Public Constructors
        public IntegrationBaseChannel(string name)
        {
            OnMessage += delegate { };
            _name = name;
        }
        #endregion

        #region Public Methods
        public abstract bool Connect();

        public abstract bool Disconnect();

        public abstract bool IsOpen();
        public abstract IIntegrationMessage MessageFactory();

        public abstract IntegrationPostResponse Post(IIntegrationMessage message);

        public abstract void SetConfig();
        #endregion
    }
}
﻿using System.Collections.Generic;
using System.Data;

namespace SAK.DAL
{
    public class DbCommand
    {
        #region Private Fields
        private string _commandText = string.Empty;
        private CommandType _commandType = CommandType.Text;
        private List<DbCommandParameter> _parameters = new List<DbCommandParameter>();
        private string source = "DbCommand";
        #endregion

        #region Public Properties
        public string CommandText
        {
            get { return _commandText; }
            set
            {
                if (value == null)
                {
                    _commandText = string.Empty;
                }
                else
                {
                    _commandText = value;
                }
            }
        }
        public CommandType CommandType
        {
            get { return _commandType; }
            set { _commandType = value; }
        }
        public List<DbCommandParameter> Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }
        public string Source
        {
            get { return source; }
            set
            {
                if (value == null)
                {
                    source = string.Empty;
                }
                else
                {
                    source = value;
                }
            }
        }
        #endregion

        #region Public Constructors
        public DbCommand()
        {
        }

        public DbCommand(string cmdText, CommandType cmdType)
        {
            CommandText = cmdText;
            CommandType = cmdType;
        }
        #endregion

        #region Public Methods
        public void AddParameter(string name, object value, ParameterDirection direction = ParameterDirection.Input)
        {
            Parameters.Add(new DbCommandParameter() { Name = name, Value = value });
        }

        public override string ToString()
        {
            return string.Format("DbCommand=[[CommandText={0}]\t[CommandType={1}]\t[Source={2}]\t[Parameters={3}]]", CommandText, CommandType, Source, string.Join("\t", Parameters));
        }
        #endregion

    }
}
﻿using System;
using System.Data;

namespace SAK.DAL
{
    public class DbCommandParameter
    {
        #region Public Properties
        public DbType DataType
        {
            get
            {
                return DbTypeMap.GetDbType(Value);
            }
        }

        public ParameterDirection Direction { get; set; }
        public string Name { get; set; }
        public object Value { get; set; }
        #endregion

        #region Public Constructors
        public DbCommandParameter()
        {
            Direction = ParameterDirection.Input;
            Value = DBNull.Value;
            Name = string.Empty;
        }
        #endregion

        #region Public Methods
        public void Add(string name, object value)
        {
            Name = name;
            Value = value;
        }
        #endregion

        #region Public Methods
        public override string ToString()
        {
            return string.Format("DbCommandParameter=[[Name={0}]\t[DataType={1}]\t[Dreiction={2}]\t[Value={3}]]", Name, DataType, Direction, Value);
        }

        #endregion
    }
}
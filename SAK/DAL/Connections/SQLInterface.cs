﻿using SAK.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAK.DAL
{
    public class SQLInterface : IDbInterface, IDisposable
    {

        #region Private Fields
        private DbConnection _conn = null;
        private string _connectionString = string.Empty;
        #endregion

        #region Public Properties
        public string ConnectionString
        {
            get
            {
                return _connectionString;
            }
        }
        #endregion

        #region Public Constructors
        public SQLInterface(string connectionString)
        {
            _connectionString = connectionString;
        }
        #endregion

        #region Public Methods
        public void Dispose()
        {
            if (_conn != null)
            {
                if (_conn.State != ConnectionState.Closed)
                {
                    _conn.Close();
                }
            }
        }

        public int ExecuteNonQuery(DbCommand command)
        {
            int ret = -1;

            using (var cmd = CreateCommand())
            {
                cmd.CommandText = command.CommandText;
                cmd.CommandType = command.CommandType;
                foreach (DbCommandParameter item in command.Parameters)
                {
                    DbParameter par = cmd.CreateParameter();
                    par.ParameterName = item.Name;
                    par.DbType = item.DataType;
                    par.Value = item.Value;
                    par.Direction = item.Direction;

                    cmd.Parameters.Add(par);
                }

                ret = cmd.ExecuteNonQuery();
            }

            return ret;
        }

        public int ExecuteNonQuery(DbCommand command, out Dictionary<string, object> outputParameters)
        {
            int ret = -1;
            outputParameters = new Dictionary<string, object>();

            using (var cmd = CreateCommand())
            {
                cmd.CommandText = command.CommandText;
                cmd.CommandType = command.CommandType;
                foreach (DbCommandParameter item in command.Parameters)
                {
                    DbParameter par = cmd.CreateParameter();
                    par.ParameterName = item.Name;
                    par.DbType = item.DataType;
                    par.Value = item.Value;
                    par.Direction = item.Direction;

                    cmd.Parameters.Add(par);
                }

                ret = cmd.ExecuteNonQuery();

                outputParameters = new Dictionary<string, object>();

                foreach (DbCommandParameter par in command.Parameters.Where(p => p.Direction != ParameterDirection.Input))
                {
                    var item = cmd.Parameters[par.Name];
                    outputParameters.Add(item.ParameterName, item.Value);
                }
            }


            return ret;
        }

        public IDataReader ExecuteReader(DbCommand command)
        {
            using (var cmd = CreateCommand())
            {
                cmd.CommandText = command.CommandText;
                cmd.CommandType = command.CommandType;
                foreach (DbCommandParameter item in command.Parameters)
                {
                    DbParameter par = cmd.CreateParameter();
                    par.ParameterName = item.Name;
                    par.DbType = item.DataType;
                    par.Value = item.Value;
                    par.Direction = item.Direction;

                    cmd.Parameters.Add(par);
                }

                return cmd.ExecuteReader();
            }

        }
        public object ExecuteScalar(DbCommand command)
        {
            object ret = null;

            using (var cmd = CreateCommand())
            {
                cmd.CommandText = command.CommandText;
                cmd.CommandType = command.CommandType;
                foreach (DbCommandParameter item in command.Parameters)
                {
                    DbParameter par = cmd.CreateParameter();
                    par.ParameterName = item.Name;
                    par.DbType = item.DataType;
                    par.Value = item.Value;
                    par.Direction = item.Direction;

                    cmd.Parameters.Add(par);
                }

                ret = cmd.ExecuteScalar();
            }


            return ret;
        }

        public object ExecuteScalar(DbCommand command, out Dictionary<string, object> outputParameters)
        {
            object ret = null;
            outputParameters = new Dictionary<string, object>();

            using (var cmd = CreateCommand())
            {
                cmd.CommandText = command.CommandText;
                cmd.CommandType = command.CommandType;
                foreach (DbCommandParameter item in command.Parameters)
                {
                    DbParameter par = cmd.CreateParameter();
                    par.ParameterName = item.Name;
                    par.DbType = item.DataType;
                    par.Value = item.Value;
                    par.Direction = item.Direction;

                    cmd.Parameters.Add(par);
                }

                ret = cmd.ExecuteScalar();

                outputParameters = new Dictionary<string, object>();

                foreach (DbCommandParameter par in command.Parameters.Where(p => p.Direction != ParameterDirection.Input))
                {
                    var item = cmd.Parameters[par.Name];
                    outputParameters.Add(item.ParameterName, item.Value);
                }
            }

            return ret;
        }
        #endregion

        #region Private Methods
        private System.Data.Common.DbCommand CreateCommand()
        {
            if (_conn == null || _conn.State == ConnectionState.Closed || _conn.State == ConnectionState.Broken)
            {
                _conn = new System.Data.SqlClient.SqlConnection(_connectionString);

                _conn.Open();
            }

            return _conn.CreateCommand();
        }
        #endregion
    }
}

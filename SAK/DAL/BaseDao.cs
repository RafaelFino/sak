﻿using SAK.Config;
using SAK.Enumerators;
using SAK.Interfaces;
using SAK.Logger;
using SAK.PerformanceMonitor;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace SAK.DAL
{
    public class BaseDao : IDisposable
    {

        #region Private Fields
        private DbConnectionConfigElement _config;
        private IDbInterface _db;
        private bool _logDebug = false;        
        #endregion

        #region Public Properties
        public string ConnectionString
        {
            get { return _db.ConnectionString; }
        }
        public bool LogDebug
        {
            get { return _logDebug; }
            set { _logDebug = value; }
        }
        #endregion

        #region Public Constructors
        public BaseDao(string name)
        {
            _config = ConfigManager.LoadDBConfig(name);

            if (_config == null)
            {
                string errorMessage = string.Format("[BTG.Shared.DAL.BaseDao] Config key not found for {0}", name);
                Exception ex = new Exception(errorMessage);
                LoggerManager.Instance.Error(ex, errorMessage);
                throw ex;
            }
            else
            {
                DbInterfaceType dbType;

                if (!Enum.TryParse<DbInterfaceType>(_config.DbInterfaceType, true, out dbType))
                {
                    dbType = DbInterfaceType.InfrastructureInterface;
                }

                _logDebug = _config.LogDebug;
                _db = CreateDbInterface(_config, dbType);
            }
        }
        #endregion

        #region Public Methods
        public void Dispose()
        {
        }

        public int ExecuteNonQuery(DbCommand command, out Dictionary<string, object> outputParameters)
        {
            int ret = -1;

            try
            {
                using (IPerfCounter counter = PerfMonitor.CreateCounter(command.Source + ".Execute"))
                {
                    if (_logDebug)
                    {
                        LoggerManager.Instance.Debug("[{0}.Execute] Try to execute: {1}", command.Source, command);
                    }

                    ret = _db.ExecuteNonQuery(command, out outputParameters);
                }
            }
            catch (Exception ex)
            {
                LoggerManager.Instance.Error(ex, "[{0}.Execute] Error to try execute a database command -> {1}", command.Source, command);
                throw;
            }

            return ret;
        }

        public int ExecuteNonQuery(DbCommand command)
        {
            int ret = -1;

            try
            {
                using (IPerfCounter counter = PerfMonitor.CreateCounter(command.Source + ".Execute"))
                {
                    if (_logDebug)
                    {
                        LoggerManager.Instance.Debug("[{0}.Execute] Try to execute: {1}", command.Source, command);
                    }

                    ret = _db.ExecuteNonQuery(command);
                }
            }
            catch (Exception ex)
            {
                LoggerManager.Instance.Error(ex, "[{0}.Execute] Error to try execute a database command -> {1}", command.Source, command);
                throw;
            }

            return ret;
        }

        public List<T> ExecuteReader<T>(DbCommand command, Func<IDataRecord, T> parser)
        {
            List<T> ret = new List<T>();

            try
            {
                using (IPerfCounter counter = PerfMonitor.CreateCounter(command.Source + ".ExecuteReader"))
                {
                    using (IDataReader dr = _db.ExecuteReader(command))
                    {
                        while (dr.Read())
                        {
                            ret.Add(parser(dr));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerManager.Instance.Error(ex, "[{0}.ExecuteReader] Error to try execute a database command -> {1}", command.Source, command);
                throw;
            }

            return ret;
        }

        public T ExecuteReaderFirst<T>(DbCommand command, Func<IDataRecord, T> parser)
        {
            T ret = default(T);
            try
            {
                using (IPerfCounter counter = PerfMonitor.CreateCounter(command.Source + ".ExecuteReaderFirst"))
                {
                    using (IDataReader dr = _db.ExecuteReader(command))
                    {
                        if (dr.Read())
                        {
                            ret = parser(dr);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerManager.Instance.Error(ex, "[{0}.ExecuteReaderFirst] Error to try execute a database command -> {1}", command.Source, command);
                throw;
            }

            return ret;
        }

        public object ExecuteScalar(DbCommand command, out Dictionary<string, object> outputParameters)
        {
            object ret = null;

            try
            {
                using (IPerfCounter counter = PerfMonitor.CreateCounter(command.Source + ".ExecuteScalar"))
                {
                    if (_logDebug)
                    {
                        LoggerManager.Instance.Debug("[{0}.ExecuteScalar] Try to execute: {1}", command.Source, command);
                    }

                    ret = _db.ExecuteScalar(command, out outputParameters);
                }
            }
            catch (Exception ex)
            {
                LoggerManager.Instance.Error(ex, "[{0}.ExecuteScalar] Error to try execute a database command -> {1}", command.Source, command);
                throw;
            }

            return ret;
        }

        public object ExecuteScalar(DbCommand command)
        {
            object ret = null;

            try
            {
                using (IPerfCounter counter = PerfMonitor.CreateCounter(command.Source + ".ExecuteScalar"))
                {
                    if (_logDebug)
                    {
                        LoggerManager.Instance.Debug("[{0}.ExecuteScalar] Try to execute: {1}", command.Source, command);
                    }

                    ret = _db.ExecuteScalar(command);
                }
            }
            catch (Exception ex)
            {
                LoggerManager.Instance.Error(ex, "[{0}.ExecuteScalar] Error to try execute a database command -> {1}", command.Source, command);
                throw;
            }

            return ret;
        }
        #endregion

        #region Private Methods
        private IDbInterface CreateDbInterface(DbConnectionConfigElement config, DbInterfaceType type)
        {
            IDbInterface ret = null;

            switch (type)
            {
                case DbInterfaceType.SQLInterface:
                    {
                        ret = new SQLInterface(config.ConnectionString);
                        break;
                    }
                case DbInterfaceType.OracleInterface:
                    {
                        ret = new OracleInterface(config.ConnectionString);
                        break;
                    }
                default:
                    {
                        break;
                    }
            }

            return ret;
        }
        #endregion
    }
}
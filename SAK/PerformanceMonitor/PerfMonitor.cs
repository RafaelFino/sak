﻿using SAK.Config;
using SAK.Enumerators;
using SAK.Interfaces;
using SAK.Logger;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SAK.PerformanceMonitor
{
    public class PerfMonitor : IDisposable
    {

        #region Public Fields
        public Action<IPerfCounter[]> DetailedReport;
        public Action<string> Report;
        #endregion

        #region Private Fields
        private static readonly ConcurrentDictionary<PerfCounterType, Type> _counterTypes = new ConcurrentDictionary<PerfCounterType, Type>(
            new Dictionary<PerfCounterType, Type>()
            {
                {PerfCounterType.Default, typeof(DefaultCounter)},
                {PerfCounterType.Empty, typeof(EmptyCounter)}
            });

        private static PerfCounterType _counterType = PerfCounterType.Empty;
        private static PerfMonitor _instance = null;
        private ConcurrentDictionary<string, IPerfCounter> _counters;
        private TimeSpan _intervalToReport;
        private AutoResetEvent _reportHolder = null;
        private bool _resetCounters = true;
        private HardwareMonitor _hardwareMonitor;
        #endregion

        #region Public Properties
        public static PerfCounterType CounterType
        {
            get { return _counterType; }
        }

        public static PerfMonitor Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new PerfMonitor();
                }

                return _instance;
            }
        }

        public bool IsActive
        {
            get
            {
                return (_reportHolder != null);
            }
        }
        public bool ResetCounters
        {
            get { return _resetCounters; }
            set { _resetCounters = value; }
        }
        #endregion

        #region Private Constructors
        private PerfMonitor()
        {
            _intervalToReport = new TimeSpan(0, 5, 0);
            _counters = new ConcurrentDictionary<string, IPerfCounter>();

            PerfMonitorConfigElement config = ConfigManager.LoadPerfMonitorConfig();

            if (config != null)
            {
                if (config.Active)
                {
                    _counterType = config.CounterType;
                    _resetCounters = config.ResetCounter;

                    if (config.UseHardwareMonitor)
                    {
                        _hardwareMonitor = new HardwareMonitor();
                    }

                    Start(new TimeSpan(0, 0, config.IntervalToReport));
                }
            }
        }
        #endregion

        #region Public Methods
        public static IPerfCounter CreateCounter(string counterName)
        {
            IPerfCounter ret;

            try
            {
                ret = (IPerfCounter)Activator.CreateInstance(
                                                            _counterTypes[_counterType],
                                                            new Object[] { counterName }
                                                            );

                if (ret != null)
                {
                    Instance.Insert(ret);
                }
            }
            catch (Exception ex)
            {
                LoggerManager.Instance.Error(ex, "Error to try create an IPerformanceCounter instance to {0}", _counterType);
                ret = null;
            }

            return ret;
        }

        public void Clear()
        {
            foreach (IPerfCounter counter in _counters.Values)
            {
                counter.Clear();
            }
        }

        public string GetReport()
        {
            return string.Format("PerformanceMonitor [{2}]{3}:{0}\t{1}",
                   Environment.NewLine,
                    string.Join(Environment.NewLine + "\t", _counters.Values.Select(p => p.GetReport())),
                    DateTime.Now,
                    _hardwareMonitor != null ? _hardwareMonitor.GetReport() : string.Empty
                    );
        }

        void IDisposable.Dispose()
        {
            _reportHolder.Set();
        }

        public void Insert(IPerfCounter counter)
        {
            if (IsActive)
            {
                counter.IncrementActiveEvents();

                _counters.AddOrUpdate(counter.Name,
                    counter,
                    (k, v) =>
                    {
                        v.IncrementActiveEvents();
                        return v;
                    });
            }
        }

        public void InsertFinishedCounter(IPerfCounter counter, bool decraseProcess = false)
        {
            if (IsActive)
            {
                _counters.AddOrUpdate(counter.Name, counter, (k, v) =>
                {
                    v.DecrementActiveEvents();
                    v.Increment(counter.ElapsedTime, counter.Counter);
                    return v;
                });
            }
        }
        public void Start(TimeSpan intervalToReport)
        {
            _intervalToReport = intervalToReport;

            _reportHolder = new AutoResetEvent(false);

            Task.Factory.StartNew(ProcessReportEvents);
        }
        #endregion

        #region Private Methods
        private void ProcessReportEvents()
        {
            while (!_reportHolder.WaitOne(_intervalToReport))
            {
                string report = GetReport();

                if (Report != null)
                {
                    Report(report);
                }

                if (DetailedReport != null)
                {
                    IPerfCounter[] counters = _counters.Values.ToArray();

                    DetailedReport(counters);
                }

                if (_resetCounters)
                {
                    Clear();
                }
            }
        }
        #endregion
    }
}
﻿namespace SAK.PerformanceMonitor
{
    public class EmptyCounter : BaseCounter
    {
        #region Public Constructors
        public EmptyCounter(string name)
            : base(name)
        {
        }

        #endregion

        #region Public Methods
        public override void Increment(long elapsedTime, long value)
        {
        }

        public override void IncrementEvents(long value)
        {
        }

        public override void IncrementTime(long elapsedTime)
        {
        }

        public override void IncrementActiveEvents(long value = 1)
        {
        }

        public override void DecrementActiveEvents(long value = 1)
        {
        }
        #endregion
    }
}
﻿using SAK.Interfaces;
using System;
using System.Threading;

namespace SAK.PerformanceMonitor
{
    public abstract class BaseCounter : IPerfCounter
    {
        #region Protected Fields
        protected long _counter = 1;
        protected long _elapsedTime = 0;
        protected long _maxTimeEvent = 0;
        protected long _minTimeEvent = 0;
        protected string _name = string.Empty;
        protected long _max = 0;
        protected long _active = 1;
        #endregion

        #region Public Properties
        public decimal AvarageTime
        {
            get
            {
                return Convert.ToDecimal(_elapsedTime) / Convert.ToDecimal(_counter == 0 ? 1 : _counter);
            }
        }

        public long Counter
        {
            get { return _counter; }
            set { _counter = value; }
        }

        public long Max
        {
            get { return _max; }
        }

        public long ActiveEvents
        {
            get { return _active; }
        }

        public long ElapsedTime
        {
            get { return _elapsedTime; }
            set { _elapsedTime = value; }
        }

        public long MaxTimeEvent
        {
            get { return _maxTimeEvent; }
        }

        public long MinTimeEvent
        {
            get { return _minTimeEvent; }
        }

        public string Name
        {
            get { return _name; }
        }

        public decimal Ratio
        {
            get
            {
                return Convert.ToDecimal(_counter) / Convert.ToDecimal(_elapsedTime == 0 ? 1 : _elapsedTime);
            }
        }
        #endregion

        #region Public Constructors
        public BaseCounter(string name)
        {
            _name = name;
            _active = 1;            
        }

        #endregion

        #region Public Methods
        public virtual void Clear()
        {
            _counter = 0;
            _elapsedTime = 0;
        }

        public virtual void Dispose()
        {            
        }

        public string GetReport()
        {
            return string.Format("PerformanceCounter: [Name={0}]\t[TotalTimeElapsed={1}]\t[Events={2}]\t[MaxTime={3}]\t[MinTime={4}]\t[Ratio={5}]\t[AvarageTime={6}]\t[ActiveProcess={7}]\t[MaxActiveProcess={8}]",
                _name.PadRight(64, ' '),
                _elapsedTime.ToString("D16"),
                _counter.ToString("D16"),
                _maxTimeEvent.ToString("D16"),
                _minTimeEvent.ToString("D16"),
                Ratio.ToString("000000.0000"),
                AvarageTime.ToString("000000.0000"),
                _active.ToString("D16"),
                _max.ToString("D16"));
        }

        public abstract void Increment(long elapsedTime, long value);

        public abstract void IncrementEvents(long value);

        public abstract void IncrementTime(long elapsedTime);

        public abstract void IncrementActiveEvents(long value = 1);

        public abstract void DecrementActiveEvents(long value = 1);

        public virtual void Restart()
        {
            Clear();
        }

        public override string ToString()
        {
            return GetReport();
        }

        #endregion

        #region Protected Methods
        protected void SetElapsedTimeValues(long elapsedTime)
        {
            if (elapsedTime > _maxTimeEvent)
            {
                _maxTimeEvent = elapsedTime;
            }

            if (elapsedTime < _minTimeEvent)
            {
                _minTimeEvent = elapsedTime;
            }
        }
        #endregion
    }
}
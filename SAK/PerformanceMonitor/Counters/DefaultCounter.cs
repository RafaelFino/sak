﻿using System;
using System.Diagnostics;
using System.Threading;

namespace SAK.PerformanceMonitor
{
    public class DefaultCounter : BaseCounter
    {
        #region Private Fields
        private Stopwatch _sw = new Stopwatch();
        #endregion

        #region Public Constructors
        public DefaultCounter(string name)
            : base(name)
        {
            _sw.Start();
        }

        #endregion

        #region Public Methods
        public override void Clear()
        {
            _counter = 0;
            _elapsedTime = 0;
            _sw.Restart();
        }

        public override void Dispose()
        {
            _sw.Stop();
            IncrementTime(_sw.ElapsedMilliseconds);            
            PerfMonitor.Instance.InsertFinishedCounter(this, true);
        }

        public override void Increment(long elapsedTime, long value)
        {
            IncrementTime(elapsedTime);
            IncrementEvents(value);
        }

        public override void IncrementEvents(long value)
        {
            Interlocked.Add(ref _counter, value);
        }

        public override void IncrementTime(long elapsedTime)
        {
            Interlocked.Add(ref _elapsedTime, elapsedTime);

            SetElapsedTimeValues(elapsedTime);
        }

        public override void Restart()
        {
            _counter = 0;
            _elapsedTime = 0;
            _sw.Restart();
        }

        public override void IncrementActiveEvents(long value = 1)
        {
            _active = Interlocked.Add(ref _active, value);

            _max = Math.Max(_max, _active);
        }

        public override void DecrementActiveEvents(long value = 1)
        {
            _active = Interlocked.Add(ref _active, (-1) * value);

            if (_active < 0)
            {
                _active = 0;
            }
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAK.PerformanceMonitor
{
    public class HardwareMonitor
    {
        #region Private Fields
        private PerformanceCounter _cpuCounter;
        private PerformanceCounter _ramCounter;
        #endregion

        #region Public Methods
        public string GetReport()
        {
            return string.Format(" [CPU: {0}%] [Free RAM: {1} MB]", Math.Round(_cpuCounter.NextValue(), 2).ToString("00.00"), _ramCounter.NextValue());
        }

        public HardwareMonitor()
        {
            _cpuCounter = new PerformanceCounter("Processor", "% Processor Time", "_Total", true);
            _ramCounter = new PerformanceCounter("Memory", "Available MBytes", true);
        }
        #endregion
    }
}

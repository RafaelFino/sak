﻿using SAK.Interfaces;
using System;
using System.Xml.Serialization;

namespace SAK.Serializer
{
    public class ProtobufSerializer<T> : ISerializer<T>
    {
        #region Public Methods
        public T Deserialize(byte[] serializationBytes)
        {
            T ret;
            using (var ms = new System.IO.MemoryStream(serializationBytes))
            {
                ret = ProtoBuf.Serializer.Deserialize<T>(ms);
            }

            return ret;
        }

        public T Deserialize(string data)
        {
            return Deserialize(Convert.FromBase64String(data));
        }

        public byte[] Serialize(T item)
        {
            byte[] b = null;
            using (var ms = new System.IO.MemoryStream())
            {
                ProtoBuf.Serializer.Serialize<T>(ms, item);
                b = new byte[ms.Position];
                var fullB = ms.GetBuffer();
                Array.Copy(fullB, b, b.Length);
            }
            return b;
        }

        public string SerializeToString(T item)
        {
            string ret = string.Empty;
            using (var ms = new System.IO.MemoryStream())
            {
                ProtoBuf.Serializer.Serialize(ms, item);
                ret = Convert.ToBase64String(ms.GetBuffer(), 0, (int)ms.Length);
            }

            return ret;
        }

        #endregion
    }
}
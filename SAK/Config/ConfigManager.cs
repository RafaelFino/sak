﻿using SAK.Logger;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAK.Config
{
    public class ConfigManager
    {
        #region Public Methods
        public static Dictionary<string, DbConnectionConfigElement> LoadDBConfig()
        {
            DbConnectionConfigSection section = (DbConnectionConfigSection)ConfigurationManager.GetSection(DbConnectionConfigSection.TagRoot);
            Dictionary<string, DbConnectionConfigElement> ret = new Dictionary<string, DbConnectionConfigElement>();

            if (section != null)
            {
                foreach (DbConnectionConfigElement item in section.Connections)
                {
                    ret.Add(item.Name, item);
                }
            }

            return ret;
        }

        public static DbConnectionConfigElement LoadDBConfig(string name)
        {
            DbConnectionConfigSection section = (DbConnectionConfigSection)ConfigurationManager.GetSection(DbConnectionConfigSection.TagRoot);
            DbConnectionConfigElement ret = null;

            if (section != null)
            {
                foreach (DbConnectionConfigElement item in section.Connections)
                {
                    if (item.Name.Equals(name))
                    {
                        ret = item;
                        break;
                    }
                }
            }

            return ret;
        }

        public static Dictionary<string, LoggerConfigElement> LoadLoggerConfig()
        {
            LoggerConfigSection section = (LoggerConfigSection)ConfigurationManager.GetSection(LoggerConfigSection.TagRoot);
            Dictionary<string, LoggerConfigElement> ret = new Dictionary<string, LoggerConfigElement>();

            if (section != null)
            {
                foreach (LoggerConfigElement item in section.Loggers)
                {
                    ret.Add(item.Name, item);
                }
            }

            return ret;
        }

        public static LoggerConfigElement LoadLoggerConfig(string name)
        {
            LoggerConfigSection section = (LoggerConfigSection)ConfigurationManager.GetSection(LoggerConfigSection.TagRoot);
            LoggerConfigElement ret = null;

            if (section != null)
            {
                foreach (LoggerConfigElement item in section.Loggers)
                {
                    if (item.Name.Equals(name))
                    {
                        ret = item;
                        break;
                    }
                }
            }

            return ret;
        }

        public static MQConfigElement LoadWMQConfig(string name)
        {
            MQConfigSection section = (MQConfigSection)ConfigurationManager.GetSection(MQConfigSection.TagRoot);

            if (section != null)
            {
                for (int i = 0; i < section.Queues.Count; i++)
                {
                    MQConfigElement configItem = section.Queues[i];

                    if (configItem.Queue.Equals(name))
                    {
                        return configItem;
                    }
                }
            }

            return null;
        }

        public static PerfMonitorConfigElement LoadPerfMonitorConfig(string name = "Default")
        {            
            PerfMonitorConfigSection section = (PerfMonitorConfigSection)ConfigurationManager.GetSection(PerfMonitorConfigSection.TagRoot);

            if (section != null)
            {
                for (int i = 0; i < section.Counters.Count; i++)
                {
                    PerfMonitorConfigElement configItem = section.Counters[i];

                    if (configItem.Name.Equals(name))
                    {
                        return configItem;
                    }
                }
            }

            return null;
        }

        public static string ReadFromAppConfig(string key, string defaultValue = "")
        {
            if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains(key))
            {
                return System.Configuration.ConfigurationManager.AppSettings[key];
            }
            else
            {
                LoggerManager.Instance.Warning("[ConfigManager.ReadFromAppConfig] Fail to try read key {0} from app settings, using {1} as default value", key, defaultValue);

                return defaultValue;
            }
        }
        #endregion

    }
}

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAK.Enumerators;

namespace SAK.Config
{
    [ConfigurationCollection(typeof(MQConfigElement))]
    public class MQConfigCollections : ConfigurationElementCollection
    {
        #region Public Indexers
        public MQConfigElement this[int idx]
        {
            get
            {
                return (MQConfigElement)BaseGet(idx);
            }
        }
        #endregion

        #region Protected Methods
        protected override ConfigurationElement CreateNewElement()
        {
            return new MQConfigElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((MQConfigElement)(element)).Queue;
        }
        #endregion
    }

    public class MQConfigElement : ConfigurationElement
    {
        #region Public Properties
        [ConfigurationProperty("autoCommit", DefaultValue = "true", IsRequired = false)]
        public bool AutoCommit
        {
            get
            {
                return (bool)this["autoCommit"];
            }
            set
            {
                this["autoCommit"] = value;
            }
        }        

        [ConfigurationProperty("channel", DefaultValue = "", IsRequired = false)]
        public string Channel
        {
            get
            {
                return (string)this["channel"];
            }
            set
            {
                this["channel"] = value;
            }
        }


        [ConfigurationProperty("serializer", DefaultValue = "DefaultXml", IsRequired = false)]
        public SerializerTypes Serializer
        {
            get
            {
                return (SerializerTypes)this["serializer"];
            }
            set
            {
                SerializerTypes ret;

                if (!Enum.TryParse<SerializerTypes>(value.ToString(), out ret))
                {
                    ret = SerializerTypes.DefaultXml;
                }

                this["serializer"] = ret;
            }
        }

        [ConfigurationProperty("host", DefaultValue = "", IsRequired = true)]
        public string Host
        {
            get
            {
                return (string)this["host"];
            }
            set
            {
                this["host"] = value;
            }
        }

        [ConfigurationProperty("port", DefaultValue = "1414", IsRequired = false)]
        public string Port
        {
            get
            {
                return (string)this["port"];
            }
            set
            {
                this["port"] = value;
            }
        }
        [ConfigurationProperty("queueManager", DefaultValue = "", IsRequired = false)]
        public string QueueManager
        {
            get
            {
                return (string)this["queueManager"];
            }
            set
            {
                this["queueManager"] = value;
            }
        }

        [ConfigurationProperty("queue", DefaultValue = "", IsRequired = true)]
        public string Queue
        {
            get
            {
                return (string)this["queue"];
            }
            set
            {
                this["queue"] = value;
            }
        }

        [ConfigurationProperty("type", DefaultValue = "BTG.Shared.Integration.Channels.NBlocksMQ", IsRequired = true)]
        public string QueueType
        {
            get
            {
                return (string)this["type"];
            }
            set
            {
                this["type"] = value;
            }
        }

        [ConfigurationProperty("mode", DefaultValue = "Writer", IsRequired = true)]
        public QueueMode Mode
        {
            get
            {
                return (QueueMode)this["mode"];
            }
            set
            {
                QueueMode ret;

                if (!Enum.TryParse<QueueMode>(value.ToString(), out ret))
                {
                    ret = QueueMode.Writer;
                }

                this["mode"] = ret;
            }
        }
        #endregion

        #region Public Constructors
        public MQConfigElement()
        {
        }
        #endregion
    }

    public class MQConfigSection : ConfigurationSection
    {
        #region Public Fields
        public const string TagRoot = "queueConfig";
        #endregion

        #region Public Properties
        [ConfigurationProperty("queues")]
        public MQConfigCollections Queues
        {
            get
            {
                return ((MQConfigCollections)(base["queues"]));
            }
        }
        #endregion

        #region Public Constructors
        public MQConfigSection()
        { }
        #endregion
    }
}

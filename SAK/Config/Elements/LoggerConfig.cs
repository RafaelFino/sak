﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAK.Config
{
    [ConfigurationCollection(typeof(LoggerConfigElement))]
    public class LoggerConfigCollections : ConfigurationElementCollection
    {
        #region Public Indexers
        public LoggerConfigElement this[int idx]
        {
            get
            {
                return (LoggerConfigElement)BaseGet(idx);
            }
        }
        #endregion

        #region Protected Methods
        protected override ConfigurationElement CreateNewElement()
        {
            return new LoggerConfigElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((LoggerConfigElement)(element)).Name;
        }
        #endregion
    }

    public class LoggerConfigElement : ConfigurationElement
    {
        #region Public Properties
        [ConfigurationProperty("name", DefaultValue = "", IsRequired = true)]
        public string Name
        {
            get
            {
                return (string)this["name"];
            }
            set
            {
                this["name"] = value;
            }
        }

        [ConfigurationProperty("type", DefaultValue = "BTG.Shared.Logger.EmptyLogger", IsRequired = false)]
        public string Type
        {
            get
            {
                return (string)this["type"];
            }
            set
            {
                this["type"] = value;
            }
        }

        [ConfigurationProperty("subscribePerformanceReport", DefaultValue = "false", IsRequired = false)]
        public bool SubscribePerformanceReport
        {
            get
            {
                return (bool)this["subscribePerformanceReport"];
            }
            set
            {
                this["subscribePerformanceReport"] = value;
            }
        }

        [ConfigurationProperty("logInfo", DefaultValue = "true", IsRequired = false)]
        public bool LogInfo
        {
            get
            {
                return (bool)this["logInfo"];
            }
            set
            {
                this["logInfo"] = value;
            }
        }

        [ConfigurationProperty("logDebug", DefaultValue = "true", IsRequired = false)]
        public bool LogDebug
        {
            get
            {
                return (bool)this["logDebug"];
            }
            set
            {
                this["logDebug"] = value;
            }
        }

        [ConfigurationProperty("logWarning", DefaultValue = "true", IsRequired = false)]
        public bool LogWarning
        {
            get
            {
                return (bool)this["logWarning"];
            }
            set
            {
                this["logWarning"] = value;
            }
        }

        [ConfigurationProperty("logPerformance", DefaultValue = "true", IsRequired = false)]
        public bool LogPerformance
        {
            get
            {
                return (bool)this["logPerformance"];
            }
            set
            {
                this["logPerformance"] = value;
            }
        }

        [ConfigurationProperty("logAppError", DefaultValue = "true", IsRequired = false)]
        public bool LogAppError
        {
            get
            {
                return (bool)this["logAppError"];
            }
            set
            {
                this["logAppError"] = value;
            }
        }

        [ConfigurationProperty("logError", DefaultValue = "true", IsRequired = false)]
        public bool LogError
        {
            get
            {
                return (bool)this["logError"];
            }
            set
            {
                this["logError"] = value;
            }
        }

        [ConfigurationProperty("logCritical", DefaultValue = "true", IsRequired = false)]
        public bool LogCritical
        {
            get
            {
                return (bool)this["logCritical"];
            }
            set
            {
                this["logCritical"] = value;
            }
        }
        #endregion

        #region Public Constructors
        public LoggerConfigElement()
        {
        }
        #endregion
    }

    public class LoggerConfigSection : ConfigurationSection
    {
        #region Public Fields
        public const string TagRoot = "loggerConfig";
        #endregion

        #region Public Properties
        [ConfigurationProperty("loggers")]
        public LoggerConfigCollections Loggers
        {
            get
            {
                return ((LoggerConfigCollections)(base["loggers"]));
            }
        }
        #endregion

        #region Public Constructors
        public LoggerConfigSection()
        { }
        #endregion
    }

    class LoggerConfig
    {
    }
}

﻿using SAK.Enumerators;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAK.Config
{
    [ConfigurationCollection(typeof(PerfMonitorConfigElement))]
    public class PerfMonitorConfigCollections : ConfigurationElementCollection
    {

        #region Public Indexers
        public PerfMonitorConfigElement this[int idx]
        {
            get
            {
                return (PerfMonitorConfigElement)BaseGet(idx);
            }
        }
        #endregion

        #region Protected Methods
        protected override ConfigurationElement CreateNewElement()
        {
            return new PerfMonitorConfigElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((PerfMonitorConfigElement)(element)).Name;
        }
        #endregion

    }

    public class PerfMonitorConfigElement : ConfigurationElement
    {
        #region Public Properties
        [ConfigurationProperty("active", DefaultValue = "false", IsRequired = false)]
        public bool Active
        {
            get
            {
                return (bool)this["active"];
            }
            set
            {
                this["active"] = value;
            }
        }

        [ConfigurationProperty("useHardwareMonitor", DefaultValue = "false", IsRequired = false)]
        public bool UseHardwareMonitor
        {
            get
            {
                return (bool)this["useHardwareMonitor"];
            }
            set
            {
                this["useHardwareMonitor"] = value;
            }
        }
        [ConfigurationProperty("counterType", DefaultValue = "Default", IsRequired = false)]
        public PerfCounterType CounterType
        {
            get
            {
                return (PerfCounterType)this["counterType"];
            }
            set
            {
                PerfCounterType ret;

                if (!Enum.TryParse<PerfCounterType>(value.ToString(), out ret))
                {
                    ret = PerfCounterType.Empty;
                }

                this["counterType"] = ret;
            }
        }
        [ConfigurationProperty("intervalToReport", DefaultValue = "300", IsRequired = false)]
        public int IntervalToReport
        {
            get
            {
                return (int)this["intervalToReport"];
            }
            set
            {
                this["intervalToReport"] = value;
            }
        }
        [ConfigurationProperty("name", DefaultValue = "Default", IsRequired = false)]
        public string Name
        {
            get
            {
                return (string)this["name"];
            }
            set
            {
                this["name"] = value;
            }
        }
        [ConfigurationProperty("resetCounter", DefaultValue = "true", IsRequired = false)]
        public bool ResetCounter
        {
            get
            {
                return (bool)this["resetCounter"];
            }
            set
            {
                this["resetCounter"] = value;
            }
        }
        #endregion

        #region Public Constructors
        public PerfMonitorConfigElement()
        {

        }
        #endregion
    }

    public class PerfMonitorConfigSection : ConfigurationSection
    {

        #region Public Fields
        public const string TagRoot = "performanceMonitorConfig";
        #endregion

        #region Public Properties
        [ConfigurationProperty("counters")]
        public PerfMonitorConfigCollections Counters
        {
            get
            {
                return ((PerfMonitorConfigCollections)(base["counters"]));
            }
        }
        #endregion

        #region Public Constructors
        public PerfMonitorConfigSection()
        {
        }
        #endregion

    }
}

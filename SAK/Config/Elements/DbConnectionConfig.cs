﻿using SAK.Enumerators;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAK.Config
{
    [ConfigurationCollection(typeof(DbConnectionConfigElement))]
    public class DbConnectionConfigCollections : ConfigurationElementCollection
    {
        #region Public Indexers
        public DbConnectionConfigElement this[int idx]
        {
            get
            {
                return (DbConnectionConfigElement)BaseGet(idx);
            }
        }
        #endregion

        #region Protected Methods
        protected override ConfigurationElement CreateNewElement()
        {
            return new DbConnectionConfigElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((DbConnectionConfigElement)(element)).Name;
        }
        #endregion
    }

    public class DbConnectionConfigElement : ConfigurationElement
    {
        #region Public Properties
        [ConfigurationProperty("connectionString", DefaultValue = "", IsRequired = true)]
        public string ConnectionString
        {
            get
            {
                return (string)this["connectionString"];
            }
            set
            {
                this["connectionString"] = value;
            }
        }

        [ConfigurationProperty("dbInterfaceType", DefaultValue = "InfrastructureInterface", IsRequired = false)]
        public string DbInterfaceType
        {
            get
            {
                return (string)this["dbInterfaceType"];
            }
            set
            {
                this["dbInterfaceType"] = value;
            }
        }

        [ConfigurationProperty("name", DefaultValue = "", IsRequired = true)]
        public string Name
        {
            get
            {
                return (string)this["name"];
            }
            set
            {
                this["name"] = value;
            }
        }

        [ConfigurationProperty("logDebug", DefaultValue = "false", IsRequired = false)]
        public bool LogDebug
        {
            get
            {
                return (bool)this["logDebug"];
            }
            set
            {
                this["logDebug"] = value;
            }
        }

        [ConfigurationProperty("useSecureConnect", DefaultValue = "false", IsRequired = false)]
        public bool UseSecureConnect
        {
            get
            {
                return (bool)this["useSecureConnect"];
            }
            set
            {
                this["useSecureConnect"] = value;
            }
        }
        #endregion

        #region Public Constructors
        public DbConnectionConfigElement()
        {
        }
        #endregion
    }

    public class DbConnectionConfigSection : ConfigurationSection
    {
        #region Public Fields
        public const string TagRoot = "databaseConfig";
        #endregion

        #region Public Properties
        [ConfigurationProperty("connections")]
        public DbConnectionConfigCollections Connections
        {
            get
            {
                return ((DbConnectionConfigCollections)(base["connections"]));
            }
        }
        #endregion

        #region Public Constructors
        public DbConnectionConfigSection()
        { }
        #endregion
    }
}

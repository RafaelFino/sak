﻿using SAK.Interfaces;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SAK.Entities
{
    [DataContract]
    public class CommunicationMessage : ICommunicationMessage
    {
        #region Public Properties
        public Dictionary<string, byte[]> Attachs { get; set; }
        public string Data { get; set; }
        public string From { get; set; }
        public string Subject { get; set; }
        public string To { get; set; }
        public string ID { get; set; }
        public DateTime MessageTime { get; set; }
        #endregion
    }
}
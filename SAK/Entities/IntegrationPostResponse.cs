﻿using SAK.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAK.Entities
{
    public class IntegrationPostResponse
    {
        public IntegrationPostReturn Status { get; set; }
        public string MessageId { get; set; }
        public string Channel { get; set; }

        public IntegrationPostResponse(string channel)
        {
            Status = IntegrationPostReturn.Invalid;
            MessageId = string.Empty;
            Channel = channel;
        }

        public bool IsValid
        {
            get
            {
                return (!string.IsNullOrEmpty(MessageId) && Status == IntegrationPostReturn.Success);
            }
        }

        public override string ToString()
        {
            return string.Format("IntegrationPostResponse=[[Channel={0}]\t[MessageId={1}]\t[Status={2}]]", 
                string.IsNullOrWhiteSpace(Channel) ? string.Empty : Channel,
                string.IsNullOrWhiteSpace(MessageId) ? string.Empty : MessageId,
                Status.ToString()
                );
        }
    }
}

﻿using SAK.Interfaces;
using ProtoBuf;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.IO;
using SAK.Serializer;

namespace SAK.Entities
{
    public delegate void MessageEvent(string channel, IIntegrationMessage message);

    [DataContract]
    [ProtoContract]
    public class IntegrationMessage : IIntegrationMessage
    {
        public IntegrationMessage()
        {
            Data = string.Empty;
            From = string.Empty;
            To = string.Empty;
            ID = string.Empty;
            MessageTime = DateTime.Now;
            Header = new Dictionary<string, string>();
        }

        [DataMember]
        [ProtoMember(501, DynamicType = true)]
        public string Data { get; set; }

        [DataMember]
        [ProtoMember(502)]
        public string From { get; set; }

        [DataMember]
        [ProtoMember(506)]
        public IDictionary<string, string> Header { get; set; }

        [DataMember]
        [ProtoMember(504)]
        public string ID { get; set; }

        [DataMember]
        [ProtoMember(505)]
        public DateTime MessageTime { get; set; }

        [DataMember]
        [ProtoMember(508)]
        public string MessageType { get; set; }

        [DataMember]
        [ProtoMember(503)]
        public string To { get; set; }

        [DataMember]
        [ProtoMember(507)]
        public string User { get; set; }
        public override string ToString()
        {
            return string.Format("IntegrationMessage=[[ID={0}]\t[Time={1}]\t[From={2}]\t[To={3}]\t[Data={4}]\t[Header={5}]]", ID, MessageTime, From, To, Data, string.Join(";", Header.Select(p => p.Key + "=" + p.Value)));
        }

        public virtual string ToXML()
        {
            DefaultXmlSerializer<IntegrationMessage> serializer = new DefaultXmlSerializer<IntegrationMessage>();
            return serializer.SerializeToString(this);
        }

        public static IntegrationMessage FromXML(string xml)
        {
            DefaultXmlSerializer<IntegrationMessage> serializer = new DefaultXmlSerializer<IntegrationMessage>();
            return serializer.Deserialize(xml);
        }
    }
}
﻿using System;
using System.Data;

namespace SAK.DAL
{
    public static class DatabaseContextExtension
    {
        /// <summary>
        /// Le um valor de um registro SQL
        /// </summary>
        /// <typeparam name="T">Tipo do dado do campo</typeparam>
        /// <param name="record">registro com os dados</param>
        /// <param name="column">nome do campo da tabela</param>
        /// <param name="defaultValue">Valor em caso de campo nulo</param>
        /// <returns>um objeto do tipo T</returns>
        public static T Read<T>(this IDataRecord record, string column, T defaultValue = default(T))
        {
            var value = record[column];
            return (T)((DBNull.Value.Equals(value)) ? defaultValue : Convert.ChangeType(value, typeof(T)));
        }

        public static string ReadTrimString(this IDataRecord record, string column)
        {
            var value = record[column];
            return ((DBNull.Value.Equals(value)) ? string.Empty : value.ToString().Trim());
        }
    }
}